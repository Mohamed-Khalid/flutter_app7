class MovieResponse {
  int? page;
  List<Results>? results;

  MovieResponse({this.page, this.results});

  MovieResponse.fromJson(Map<String, dynamic> json) {
    page = json['page'];
    if (json['results'] != null) {
      results = <Results>[];
      json['results'].forEach((v) {
        results!.add(new Results.fromJson(v));
      });
    }
  }

}

class Page {
  Page.fromJson(Map<String, dynamic> map);

}

class Results {
  String? posterPath;
  String? title;
  dynamic? voteAverage;

  Results(
      {this.posterPath, this.title, this.voteAverage,});

  Results.fromJson(Map<String, dynamic> map) {
    posterPath = map['poster_path'];
    title = map['title'];
    voteAverage = map['vote_average'];
  }
}