// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter_application_7/model/dataresponse.dart';
import 'package:flutter_application_7/presentations/home/details.dart';
import 'package:flutter_application_7/repositroy/datarepositroy.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  DataRepository? _repository;
  @override
  void initState() {
    _repository = DataRepository();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: Colors.pink,
        title: Text(
          'Choose Movie',
          style: TextStyle(
              fontSize: 24, fontWeight: FontWeight.bold, color: Colors.white),
        ),
        centerTitle: true,
      ),
      body: FutureBuilder<dynamic>(
        future: _repository?.fetchRemoteData(),
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            // ignore: no_leading_underscores_for_local_identifiers, non_constant_identifier_names, unused_local_variable
            DataResponse _DataResponse = snapshot.data!;
            return GridView.builder(
              itemCount: snapshot.data.hashCode,
              itemBuilder: (context, index) {
                return Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 5.0, vertical: 5.0),
                  child: Column(
                    children: [
                      Expanded(
                        child: GestureDetector(
                          onTap: () {
                            Navigator.push(
                                (context),
                                MaterialPageRoute(
                                    builder: (context) => DetailsScreen(

                                    ),settings: RouteSettings(arguments: _DataResponse.results![index])));
                          },
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color: Colors.amber,
                            ),
                            child: Image.network(
                              'http://image.tmdb.org/t/p/w500${_DataResponse.results![index].posterPath}',
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                      ),
                      Text(
                        '${_DataResponse.results![index].title}',
                        style: TextStyle(fontWeight: FontWeight.bold),
                      )
                    ],
                  ),
                );
              },
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
              ),
            );
          } else if (snapshot.hasError) {
            return Text(snapshot.error!.toString());
          } else {
            return CircularProgressIndicator();
          }
        },
      ),
    );
  }
}
